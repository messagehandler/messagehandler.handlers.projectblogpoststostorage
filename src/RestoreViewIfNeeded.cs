using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MessageHandler;
using Yaus.Framework.EventSourcing;
using Environment = MessageHandler.Environment;

namespace ProjectBlogpostsToAzureStorage
{
    public class RestoreViewIfNeeded : IStartupTask
    {
        private readonly IBuildViews _viewBuilder;
        private readonly ITrackProjectionCodeChanges _projectionCode;
        private readonly EntityRepository<BlogpostEntry> _repository;
        private readonly string _sourceType;
        private readonly string _partitionKeyProperty;

        public RestoreViewIfNeeded(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IBuildViews viewBuilder, ITrackProjectionCodeChanges projectionCode)
        {
            _viewBuilder = viewBuilder;
            _projectionCode = projectionCode;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectBlogpostsToAzureStorageConfig>();

            var targetConnectionString = templating.Apply(config.TargetConnectionString, null, channelVariables, environmentVariables, accountVariables);
            var targetTable = templating.Apply(config.TargetTable, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _partitionKeyProperty = templating.Apply(config.PartitionKeyProperty, null, channelVariables, environmentVariables, accountVariables);

            _repository = new EntityRepository<BlogpostEntry>(targetConnectionString, targetTable);
        }

        public void Run()
        {
            var task = RunInternal();
            task.Wait();
        }

        private async Task RunInternal()
        {
            if (!await _repository.TableExists())
            {
                Trace.TraceInformation("table does not exist, restoring.");

                await _repository.CreateTable();
                await PopulateTable();
            }
            else if (await _projectionCode.HasChanges(typeof(Projection)))
            {
                Trace.TraceInformation("Projection code has changes, restoring.");
                await PopulateTable();

                await _projectionCode.RegisterChanges(typeof(Projection));

            }
        }

        private async Task PopulateTable()
        {
            var posts = await _viewBuilder.BuildAsync<BlogpostEntry>(_sourceType);
            var type = typeof (BlogpostEntry);
            var propInfo = type.GetProperty(_partitionKeyProperty);

            var list = posts.Values.ToList();
            foreach (var entity in list)
            {
                string partitionKey = propInfo.GetValue(entity).ToString();

                entity.PartitionKey = partitionKey;
                entity.RowKey = entity.BlogPostId;
                await _repository.Upsert(entity);
            }
        }


    }
}