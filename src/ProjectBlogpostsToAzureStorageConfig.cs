﻿using System.Configuration;
using MessageHandler;

namespace ProjectBlogpostsToAzureStorage
{
    /// <summary>
    /// Configuration section for blog post projection
    /// </summary>
    [HandlerConfiguration]
    public class ProjectBlogpostsToAzureStorageConfig : ConfigurationSection
    {
        /// <summary>
        /// A connection string to storage for the event source
        /// </summary>
        [ConfigurationProperty("SourceConnectionString", IsRequired = false)]
        public string SourceConnectionString
        {
            get { return this["SourceConnectionString"] as string; }
            set { this["SourceConnectionString"] = value; }
        }

        /// <summary>
        /// An identifier for the source type, used to build up the projection
        /// </summary>
        [ConfigurationProperty("SourceType", IsRequired = false)]
        public string SourceType
        {
            get { return this["SourceType"] as string; }
            set { this["SourceType"] = value; }
        }

        /// <summary>
        /// A connection string to storage for the target table
        /// </summary>
        [ConfigurationProperty("TargetConnectionString", IsRequired = false)]
        public string TargetConnectionString
        {
            get { return this["TargetConnectionString"] as string; }
            set { this["TargetConnectionString"] = value; }
        }

        /// <summary>
        /// Name of the target table
        /// </summary>
        [ConfigurationProperty("TargetTable", IsRequired = false)]
        public string TargetTable
        {
            get { return this["TargetTable"] as string; }
            set { this["TargetTable"] = value; }
        }

        /// <summary>
        /// Partition key property to use
        /// </summary>
        [ConfigurationProperty("PartitionKeyProperty", IsRequired = false)]
        public string PartitionKeyProperty
        {
            get { return this["PartitionKeyProperty"] as string; }
            set { this["PartitionKeyProperty"] = value; }
        }

    }
}
