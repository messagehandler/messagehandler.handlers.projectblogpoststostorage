﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using MessageHandler;
using Yaus.Framework.EventSourcing;
using Environment = MessageHandler.Environment;

namespace ProjectBlogpostsToAzureStorage
{
    public class ProjectBlogpostsToAzureStorageHandler :
        IStandingQuery<DynamicJsonObject>,
        IAction<DynamicJsonObject>
    {
        private readonly EntityRepository<BlogpostEntry> _repository;
        private readonly string _sourceType;
        private readonly MemoryCache _projections = new MemoryCache("Projections");
        private readonly IInvokeProjections _projectionInvoker;
        private readonly string _partitionKeyProperty;

        public ProjectBlogpostsToAzureStorageHandler(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IInvokeProjections projectionInvoker)
        {
            _projectionInvoker = projectionInvoker;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectBlogpostsToAzureStorageConfig>();

            var targetConnectionString = templating.Apply(config.TargetConnectionString, null, channelVariables, environmentVariables, accountVariables);
            var targetTable = templating.Apply(config.TargetTable, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _partitionKeyProperty = templating.Apply(config.PartitionKeyProperty, null, channelVariables, environmentVariables, accountVariables);

            _repository = new EntityRepository<BlogpostEntry>(targetConnectionString, targetTable);
        }

        public IObservable<DynamicJsonObject> Compose(IObservable<DynamicJsonObject> messages)
        {
            return from e in messages
                   where IsBlogpostEvent(e)
                   select e;
        }

        private bool IsBlogpostEvent(dynamic e)
        {
            string what = e.What;
            string blogPostId = e.BlogPostId;
            string sourceId = e.SourceId;
            string sourceType = e.SourceType;

            Trace.TraceInformation("Evaluating What:'{0}', BlogPost:'{1}', SourceId:'{2}', SourceType:'{3}'", what, blogPostId, sourceId, sourceType);

            return e.What != null && e.BlogPostId != null && e.SourceId != null && e.SourceType == _sourceType;
        }

        public async Task Action(DynamicJsonObject t)
        {
            dynamic m = t;

            string blogPostId = m.BlogPostId;
            string what = m.What;
            
            Trace.TraceInformation("Received event from blogpost '{0}'.", blogPostId);

            var eventType = GetType(what);
            if (eventType == null)
            {
                Trace.TraceInformation("Received event of unknown type '{0}' skipping", what);
                return;
            }

            var @event = Json.Decode(Json.Encode(t), eventType);
            if (@event == null)
            {
                Trace.TraceInformation("Could not deserialize json into claimed type '{0}'", what);
                return;
            }

            var partitionKey = m.Details[_partitionKeyProperty].ToString();

            var lazy = new Lazy<BlogpostEntry>(() => _repository.Get<BlogpostEntry>(partitionKey, blogPostId).Result ?? new BlogpostEntry(partitionKey, blogPostId) { BlogPostId = blogPostId });
            var cachedLazy = (Lazy<BlogpostEntry>)_projections.AddOrGetExisting(partitionKey, lazy, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(1) });
            var post = (cachedLazy ?? lazy).Value;

            Trace.TraceInformation("Retrieved blog post '{0}', going to apply projection.", blogPostId);

            _projectionInvoker.Invoke(post, @event);

            Trace.TraceInformation("Projection applied to blog post '{0}', persisting.", blogPostId);
        }

        

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.ExportedTypes.FirstOrDefault(t => t.Name == typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public async Task Complete()
        {
            foreach (var pair in _projections)
            {
                var blogpost = ((Lazy<BlogpostEntry>)pair.Value).Value;
                await _repository.Upsert(blogpost);
            }
        }

        

    }
}