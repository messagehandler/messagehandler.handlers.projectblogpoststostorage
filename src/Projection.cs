using System;
using Yaus.Contracts.BlogPosts;
using Yaus.Framework.EventSourcing;

namespace ProjectBlogpostsToAzureStorage
{
    public class Projection :
        IProjection<BlogpostEntry, BlogPostDrafted>,
        IProjection<BlogpostEntry, BlogPostPublished>,
        IProjection<BlogpostEntry, BlogPostUnpublished>,
        IProjection<BlogpostEntry, BlogPostUpdated>
    {
        public void Project(BlogpostEntry record, BlogPostDrafted msg)
        {
            record.Author = msg.Details.Author;
            record.AuthorId = msg.Details.AuthorId;
            record.BlogPostId = msg.BlogPostId;
            record.BlogId = msg.Details.BlogId;
            record.Title = msg.Details.Title;
            record.Content = msg.Details.Content;
            record.DatePosted = msg.Details.DatePosted;
            record.DatePublished = msg.Details.DatePublished;
            record.Excerpt = msg.Details.Excerpt;
            record.Tags = msg.Details.Tags;
            record.Url = msg.Details.Url;
            record.State = msg.Details.State.ToString();
        }

        public void Project(BlogpostEntry record, BlogPostPublished msg)
        {
            record.Author = msg.Details.Author;
            record.AuthorId = msg.Details.AuthorId;
            record.BlogPostId = msg.BlogPostId;
            record.BlogId = msg.Details.BlogId;
            record.Title = msg.Details.Title;
            record.Content = msg.Details.Content;
            record.DatePosted = msg.Details.DatePosted;
            record.DatePublished = msg.Details.DatePublished;
            record.Excerpt = msg.Details.Excerpt;
            record.Tags = msg.Details.Tags;
            record.Url = msg.Details.Url;
            record.State = msg.Details.State.ToString();
        }

        public void Project(BlogpostEntry record, BlogPostUnpublished msg)
        {
            record.Author = msg.Details.Author;
            record.AuthorId = msg.Details.AuthorId;
            record.BlogPostId = msg.BlogPostId;
            record.BlogId = msg.Details.BlogId;
            record.Title = msg.Details.Title;
            record.Content = msg.Details.Content;
            record.DatePosted = msg.Details.DatePosted;
            record.DatePublished = msg.Details.DatePublished;
            record.Excerpt = msg.Details.Excerpt;
            record.Tags = msg.Details.Tags;
            record.Url = msg.Details.Url;
            record.State = msg.Details.State.ToString();
        }

        public void Project(BlogpostEntry record, BlogPostUpdated msg)
        {
            record.Author = msg.Details.Author;
            record.AuthorId = msg.Details.AuthorId;
            record.BlogPostId = msg.BlogPostId;
            record.BlogId = msg.Details.BlogId;
            record.Title = msg.Details.Title;
            record.Content = msg.Details.Content;
            record.DatePosted = msg.Details.DatePosted;
            record.DatePublished = msg.Details.DatePublished;
            record.Excerpt = msg.Details.Excerpt;
            record.Tags = msg.Details.Tags;
            record.Url = msg.Details.Url;
            record.State = msg.Details.State.ToString();
        }
    }
}