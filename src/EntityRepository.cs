using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Microsoft.WindowsAzure.Storage.Table;

namespace ProjectBlogpostsToAzureStorage
{
    public class EntityRepository<T> where T : ITableEntity, new()
    {
        private readonly CloudTable _table;

        public EntityRepository(string targetConnectionString, string targetTable)
        {
            var client = GetTableClient(targetConnectionString);
            _table = client.GetTableReference(targetTable);
        }

        private static CloudTableClient GetTableClient(string connectionString)
        {
            var storageaccount = CloudStorageAccount.Parse(connectionString);
            var client = storageaccount.CreateCloudTableClient();
            client.RetryPolicy = new ExponentialRetry();
            return client;
        }

        public async Task<T> Get<T>(string partitionkey, string rowkey) where T : ITableEntity, new()
        {
            var query = new TableQuery<T>().Where(TableQuery.CombineFilters(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionkey),
                TableOperators.And,
                TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowkey)));

            TableQuerySegment<T> querySegment = null;
            var data = new List<T>();
            while (querySegment == null || querySegment.ContinuationToken != null)
            {
                querySegment = await _table.ExecuteQuerySegmentedAsync(query, querySegment != null ? querySegment.ContinuationToken : null);
                data.AddRange(querySegment);
            }

            return data.FirstOrDefault();
        }

        public async Task Upsert(T entity)
        {
            await _table.ExecuteAsync(TableOperation.InsertOrReplace(entity))
                .ContinueWith(r =>
                {
                    if (r.Exception != null)
                    {
                        r.Exception.Handle(exception =>
                        {
                            Trace.TraceError(exception.Message);
                            return true;
                        });
                    }
                });
        }

        public async Task<bool> TableExists()
        {
            return await _table.ExistsAsync();
        }

        public async Task CreateTable()
        {
            Trace.TraceInformation("Creating table.");

            await _table.CreateIfNotExistsAsync();
        }

        public async Task DeleteTable()
        {
            Trace.TraceInformation("Deleting table.");

            await _table.DeleteIfExistsAsync();
        }
    }
}