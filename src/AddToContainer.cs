using MessageHandler;
using Yaus.Framework.EventSourcing;

namespace ProjectBlogpostsToAzureStorage
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectBlogpostsToAzureStorageConfig>();
            container.UseEventSourcing(config.SourceConnectionString)
                    .EnableProjections(config.SourceConnectionString);
        }
    }
}