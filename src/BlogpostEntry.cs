using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace ProjectBlogpostsToAzureStorage
{
    public class BlogpostEntry : TableEntity
    {
        public BlogpostEntry() : base() { }

        public BlogpostEntry(string partitionKey, string rowKey) : base(partitionKey, rowKey){}

        public string BlogPostId { get; set; }
        public string BlogId { get; set; }
        public string Url { get; set; }
        public DateTime DatePosted { get; set; }
        public DateTime DatePublished { get; set; }

        public string AuthorId { get; set; }
        public string Author { get; set; }

        public string Title { get; set; }
        public string Excerpt { get; set; }
        public string Content { get; set; }
        public string State { get; set; }

        public string[] Tags { get; set; }

    }
}